package Server;

import Core.ComboServiceDetails;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;

public class ComboTCPServer
{
    public static void main(String[] args)
    {
        try
        {
            //Step 1: Set up a connection socket to listen for connections
            ServerSocket listeningSocket = new ServerSocket(ComboServiceDetails.LISTENING_PORT);

            boolean continueRunning = true;

            while(continueRunning)
            {
                //Step 2: wait for an incoming connection
                Socket dataSocket = listeningSocket.accept();

                //Step 3: Build the output and input streams
                OutputStream out = dataSocket.getOutputStream();
                PrintWriter output = new PrintWriter((new OutputStreamWriter(out)));

                InputStream in = dataSocket.getInputStream();
                Scanner input = new Scanner(new InputStreamReader(in));
                String incomingMessage = "";
                String response;

                while(!incomingMessage.equals(ComboServiceDetails.END_SESSION))
                {
                    response = null;

                    //take the information from the client
                    incomingMessage = input.nextLine();
                    System.out.println("Recevied message: " + incomingMessage);

                    String[] messageComponents = incomingMessage.split(ComboServiceDetails.COMMAND_SEPARATOR);

                    if(messageComponents[0].equals(ComboServiceDetails.ECHO))
                    {
                        StringBuffer echoMessage = new StringBuffer("");

                        if(messageComponents.length > 1)
                        {
                            echoMessage.append(messageComponents[1]);
                            //What if the user included %% in their message? It should still be echoed back to the user
                            //echo%%Hello. I love OOP%%But I really love JavaScript
                            for(int i=2; i < messageComponents.length; i++)
                            {
                                echoMessage.append(ComboServiceDetails.COMMAND_SEPARATOR);
                                echoMessage.append(messageComponents[i]);
                            }
                        }
                        response = echoMessage.toString();
                    }
                    else if(messageComponents[0].equals(ComboServiceDetails.DAYTIME))
                    {
                        response = new Date().toString();
                    }
                    else if(messageComponents[0].equals(ComboServiceDetails.STATS))
                    {
                        //TODO Homework part 1
                        //TODO Homework 2 write the client
                    }
                    else if(messageComponents[0].equals(ComboServiceDetails.END_SESSION))
                    {
                        response = ComboServiceDetails.SESSION_TERMINATED;
                    }
                    else
                    {
                        response = ComboServiceDetails.UNRECOGNISED;
                    }

                    //Send the response back
                    output.println(response);
                    output.flush();
                }
                dataSocket.close();
            }
            listeningSocket.close();
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }
}
